import 'package:flutter/material.dart';
import 'package:instaapp/src/view/main_view.dart';
import 'package:instaapp/src/view/routes.dart';

void main() => runApp(InstaApp());

class InstaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: Routes.MAIN_VIEW,
      routes: getRoutes(),
      title: 'Insta app',
      debugShowCheckedModeBanner: false,
    );
  }

  getRoutes() {
    return {Routes.MAIN_VIEW: (context) => MainView()};
  }
}
