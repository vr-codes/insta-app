import 'package:flutter/widgets.dart';
import 'package:instaapp/src/http/users.dart';
import 'package:instaapp/src/model/user.dart';

class ProfileProvider extends ChangeNotifier {
  User profile;
  Users usersHttp;

  ProfileProvider() {
    usersHttp = Users();
    usersHttp.loadMe().then((user) => updateProfile(user));
  }

  updateProfile(user) {
    this.profile = user;
    print(user);
    notifyListeners();
  }
}
