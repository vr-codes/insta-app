import 'package:flutter/widgets.dart';
import 'package:instaapp/src/http/posts.dart';
import 'package:instaapp/src/model/page.dart';
import 'package:instaapp/src/model/post.dart';
import 'package:rxdart/rxdart.dart';

class TimelineProvider extends ChangeNotifier {
  Posts postHttp = Posts();
  Page<Post> page;
  List<Post> posts;
  BehaviorSubject debounceLoad = BehaviorSubject();

  TimelineProvider() {
    posts = List();
    loadPage(0);
    debounceLoad
        .debounce((_) => TimerStream(true, Duration(milliseconds: 500)))
        .listen((_) => loadPage(page.number + 1));
  }

  void loadPage(int i) async {
    print('loading');
    page = await postHttp.loadTimeline(i);
    posts.addAll(page.content);
    notifyListeners();
  }

  void loadMore() {
    debounceLoad.add(null);
  }
}
