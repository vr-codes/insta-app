import 'package:flutter/widgets.dart';

class SearchProvider extends ChangeNotifier {
  String term = '';

  void search(String term) {
    this.term = term;
  }
}
