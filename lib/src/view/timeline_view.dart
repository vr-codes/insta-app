import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:instaapp/src/provider/timeline_provider.dart';
import 'package:instaapp/src/view/widgets/post/post_widget.dart';
import 'package:provider/provider.dart';

class TimelineView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          leading: Icon(Icons.camera_alt),
          middle: Text('Insta app'),
          trailing: Icon(Icons.send),
        ),
        child: Consumer<TimelineProvider>(
          builder: (context, value, child) {
            var scrollController = ScrollController();
            scrollController.addListener(() {
              if (scrollController.offset >=
                  scrollController.position.maxScrollExtent) {
                value.loadMore();
              }
            });
            return ListView.builder(
              itemCount: value.posts.length,
              itemBuilder: (context, i) => PostWidget(value.posts[i]),
              controller: scrollController,
            );
          },
        ),
      );
}
