import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:instaapp/src/model/post.dart';
import 'package:instaapp/src/view/widgets/post/post_actions.dart';
import 'package:instaapp/src/view/widgets/post/post_caption.dart';
import 'package:instaapp/src/view/widgets/post/post_header.dart';
import 'package:instaapp/src/view/widgets/post/post_images.dart';
import 'package:instaapp/src/view/widgets/post/post_likes.dart';

class PostWidget extends StatelessWidget {
  static const double V_PADDING = 5;
  static const double H_PADDING = 15;

  final Post post;

  PostWidget(this.post);

  @override
  Widget build(BuildContext context) => Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: V_PADDING, horizontal: H_PADDING),
              child: PostHeader(post),
            ),
            PostImages(post),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: V_PADDING, horizontal: H_PADDING),
              child: PostActions(post),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: V_PADDING, horizontal: H_PADDING),
              child: PostLikes(post),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: V_PADDING, horizontal: H_PADDING),
              child: post.caption.length > 0 ? PostCaption(post) : null,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: V_PADDING, horizontal: H_PADDING),
              child: post.commentsCount > 0
                  ? Text(
                      'View All ${post.commentsCount} Comments',
                      style: TextStyle(color: Colors.grey),
                    )
                  : null,
            ),
            SizedBox(height: 15.0)
          ],
        ),
      );
}
