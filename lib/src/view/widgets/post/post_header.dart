import 'package:flutter/cupertino.dart';
import 'package:instaapp/src/http/endpoints.dart';
import 'package:instaapp/src/model/post.dart';
import 'package:instaapp/src/view/widgets/round_photo.dart';

class PostHeader extends StatelessWidget {
  final Post post;

  PostHeader(this.post);

  @override
  Widget build(BuildContext context) => Container(
          child: Row(
        children: <Widget>[
          RoundPhoto(40.0, NetworkImage(Endpoints.userPicture(post.author.id))),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  post.author.username,
                  textScaleFactor: 1.1,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            )),
          ),
        ],
      ));
}
