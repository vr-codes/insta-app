import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:instaapp/src/model/post.dart';

class PostCaption extends StatelessWidget {
  final Post post;

  PostCaption(this.post);

  @override
  Widget build(BuildContext context) => RichText(
        text: TextSpan(
            text: '${post.author.username} ',
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            children: [
              TextSpan(
                  text: post.caption,
                  style: TextStyle(fontWeight: FontWeight.normal)),
            ]),
      );
}
