import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:instaapp/src/http/endpoints.dart';
import 'package:instaapp/src/model/post.dart';

class PostImages extends StatelessWidget {
  final Post post;

  PostImages(this.post);

  @override
  Widget build(BuildContext context) => Container(
        width: double.infinity,
        child: Image.network(Endpoints.postImages(post.id, 0), fit: BoxFit.fill,),
      );
}
