import 'package:flutter/cupertino.dart';
import 'package:instaapp/src/model/post.dart';

class PostLikes extends StatelessWidget {
  final Post post;

  PostLikes(this.post);

  @override
  Widget build(BuildContext context) => Text(
        '${post.countLikedBy} like${post.countLikedBy == 1 ? '' : 's'}',
        style: TextStyle(fontWeight: FontWeight.bold),
      );
}
