import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:instaapp/src/model/post.dart';

class PostActions extends StatelessWidget {
  final Post post;

  PostActions(this.post);

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          likeButton(),
          Icon(
            Icons.chat_bubble_outline,
            size: 30,
          ),
          Icon(
            Icons.send,
            size: 30,
          ),
          SizedBox(width: 150),
          bookmark(),
        ],
      );

  Widget bookmark() => GestureDetector(
        child: Icon(
          post.saved ? Icons.bookmark : Icons.bookmark_border,
          color: post.saved ? Colors.yellow.shade700 : Colors.black,
          size: 30,
        ),
        onTap: () => print('bookmark'),
      );

  Widget likeButton() {
    return GestureDetector(
      child: Icon(
        post.liked ? Icons.favorite : Icons.favorite_border,
        color: post.liked ? Colors.red : Colors.black,
        size: 30,
      ),
      onTap: () => print('like'),
    );
  }
}
