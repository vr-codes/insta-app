import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum BottomTabOption { home, search, add, favorite, profile }

class BottomBar {
  final homeIcon = 'images/home.png';
  final searchIcon = 'images/search.png';
  final addIcon = 'images/add.png';
  final likeIcon = 'images/like.png';
  final profileIcon = 'images/profile.png';

  CupertinoTabBar build() {
    return CupertinoTabBar(
      backgroundColor: Colors.white.withAlpha(40),
      items: [
        bottomIcon(Icons.home),
        bottomIcon(Icons.search),
        bottomIcon(Icons.add),
        bottomIcon(Icons.favorite_border),
        bottomIcon(Icons.perm_identity),
      ],
    );
  }

  BottomNavigationBarItem bottomIcon(IconData data) {
    return BottomNavigationBarItem(
      activeIcon: CircleAvatar(
        backgroundColor: Colors.grey,
        foregroundColor: Colors.white,
        child: Icon(data),
      ),
      icon: Icon(
        data,
        color: Colors.black,
      ),
    );
  }
}
