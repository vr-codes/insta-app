import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopBar {
  AppBar build(BuildContext context) => AppBar(
        title: Text('Insta app'),
        leading: Icon(Icons.camera_alt),
        actions: <Widget>[
          Icon(Icons.tv),
          SizedBox(width: 20),
          Icon(Icons.details),
          SizedBox(width: 20)
        ],
      );
}
