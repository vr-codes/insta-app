import 'package:flutter/widgets.dart';

class RoundPhoto extends StatelessWidget {
  final double size;
  final ImageProvider image;

  RoundPhoto(this.size, this.image);

  @override
  Widget build(BuildContext context) => Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: image,
            fit: BoxFit.fill,
          )));
}
