import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AddImageView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          leading: Icon(Icons.close),
          trailing: CupertinoButton(
            child: Text('Next'),
            onPressed: () {
              print('next');
            },
            padding: EdgeInsets.all(10),
          ),
          middle: DropdownButton<String>(
            value: 'image',
            onChanged: (value) {
              print(value);
            },
            items: [
              DropdownMenuItem<String>(
                child: Text('Images'),
                value: 'image',
              ),
            ],
          ),
        ),
        child: Text('add image view'),
      );
}
