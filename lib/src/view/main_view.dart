import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:instaapp/src/provider/add_image_provider.dart';
import 'package:instaapp/src/provider/favorite_provider.dart';
import 'package:instaapp/src/provider/profile_provider.dart';
import 'package:instaapp/src/provider/search_provider.dart';
import 'package:instaapp/src/provider/timeline_provider.dart';
import 'package:instaapp/src/view/add_image_view.dart';
import 'package:instaapp/src/view/base_view.dart';
import 'package:instaapp/src/view/favorite_view.dart';
import 'package:instaapp/src/view/profile_view.dart';
import 'package:instaapp/src/view/search_view.dart';
import 'package:instaapp/src/view/timeline_view.dart';
import 'package:instaapp/src/view/widgets/bottom_bar.dart';
import 'package:provider/provider.dart';

class MainView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BaseView(
        CupertinoTabScaffold(
          tabBar: BottomBar().build(),
          tabBuilder: (context, index) {
            return CupertinoTabView(
              builder: (context) {
                Widget page = Text('not defined');
                switch (index) {
                  case 0:
                    page = ChangeNotifierProvider(
                      builder: (_) => TimelineProvider(),
                      child: TimelineView(),
                    );
                    break;
                  case 1:
                    page = ChangeNotifierProvider(
                      builder: (_) => SearchProvider(),
                      child: SearchView(),
                    );
                    break;
                  case 2:
                    page = ChangeNotifierProvider(
                      builder: (_) => AddImageProvider(),
                      child: AddImageView(),
                    );
                    break;
                  case 3:
                    page = ChangeNotifierProvider(
                      builder: (_) => FavoriteProvider(),
                      child: FavoriteView(),
                    );
                    break;
                  case 4:
                    page = ChangeNotifierProvider(
                      builder: (_) => ProfileProvider(),
                      child: ProfileView(),
                    );
                    break;
                }

                return page;
              },
            );
          },
        ),
      );
}
