import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:instaapp/src/http/endpoints.dart';
import 'package:instaapp/src/model/user.dart';
import 'package:instaapp/src/provider/profile_provider.dart';
import 'package:instaapp/src/view/widgets/round_photo.dart';
import 'package:provider/provider.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => CupertinoPageScaffold(
        child: Consumer<ProfileProvider>(
            builder: (context, value, child) => value.profile != null
                ? buildProfile(value.profile)
                : Center(child: CircularProgressIndicator())),
      );

  buildProfile(User profile) {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              username(profile),
              SizedBox(height: 7.0),
              mainInfo(profile),
              SizedBox(height: 7.0),
              name(profile),
              SizedBox(height: 3.0),
              bio(profile),
            ]));
  }

  username(User profile) => Container(
      padding: EdgeInsets.all(5.0),
      child: Text(
        profile?.username ?? '',
        textScaleFactor: 1.3,
        textAlign: TextAlign.start,
        style: TextStyle(fontWeight: FontWeight.bold),
      ));

  name(User profile) => Text(
        profile?.name,
        style: TextStyle(fontWeight: FontWeight.bold),
      );

  bio(User profile) => Text(
        profile?.bio ?? '',
        textScaleFactor: .9,
      );

  mainInfo(User profile) => Container(
      padding: EdgeInsets.all(5.0),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RoundPhoto(85.0, NetworkImage(Endpoints.userPicture(profile.id))),
            Numbers(
              profile?.postCount ?? 0,
              'Posts',
            ),
            Numbers(
              profile?.followedByCount ?? 0,
              'Followers',
            ),
            Numbers(
              profile?.followingCount ?? 0,
              'Following',
            ),
          ]));
}

class Numbers extends StatelessWidget {
  final int count;
  final String label;

  Numbers(this.count, this.label);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          '$count',
          textScaleFactor: 1.6,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(
          label,
        ),
      ],
    );
  }
}
