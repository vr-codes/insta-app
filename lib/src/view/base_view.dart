import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BaseView extends StatelessWidget {
  final Widget body;

  BaseView(this.body);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(top: 2),
            child: body,
          ),
        ),
      );
}
