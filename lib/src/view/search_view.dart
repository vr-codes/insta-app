import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: TextField(
            decoration: InputDecoration(
              hintText: 'Search',
              focusedBorder: InputBorder.none,
              border: InputBorder.none,
              icon: Icon(Icons.search),
            ),
          ),
        ),
        child: Container(),
      );
}
