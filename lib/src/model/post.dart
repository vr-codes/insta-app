import 'package:instaapp/src/model/location.dart';
import 'package:instaapp/src/model/user.dart';

class Post {
  int id;
  String caption;
  Location location;
  User author;
  DateTime date;
  List<String> hashTags;
  int countLikedBy;
  bool hasTaggedUsers;
  int pictureCount;
  int commentsCount;
  bool liked;
  bool saved;

  Post.fromJson(Map<String, dynamic> js)
      : id = js['id'],
        author = User.fromJson(js['author']),
        caption = js['caption'],
        hashTags = (js['hashTags'] as List).map((v) => v as String).toList(),
        hasTaggedUsers = js['hasTaggedUsers'],
        countLikedBy = js['countLikedBy'],
        pictureCount = js['pictureCount'],
        commentsCount = js['commentsCount'],
        liked = js['liked'],
        saved = js['saved'];
}
