class User {
  int id;
  String username;
  String name;
  String site;
  String bio;
  bool open;
  int postCount;
  int followingCount;
  int followedByCount;

  User.fromJson(Map<String, dynamic> js)
      : id = js['id'],
        username = js['username'],
        name = js['name'],
        site = js['site'],
        bio = js['bio'],
        open = js['open'],
        postCount = js['postCount'],
        followingCount = js['followingCount'],
        followedByCount = js['followedByCount'];
}
