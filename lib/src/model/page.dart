class Page<T> {
  List<T> content;
  int totalPages;
  int totalElements;
  bool last;
  int number;

  Page.fromJson(Map<String, dynamic> js, T Function(Map<String, dynamic>) parser)
      : last = js['last'],
        number = js['number'],
        totalElements = js['totalElements'],
        totalPages = js['totalPages'],
        content = (js['content']as List).map((v) => parser(v)).toList();
}
