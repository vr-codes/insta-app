import 'dart:convert';

import 'package:http/http.dart';
import 'package:instaapp/src/http/endpoints.dart';
import 'package:instaapp/src/model/page.dart';
import 'package:instaapp/src/model/post.dart';

class Posts {
  Client _client = Client();

  loadTimeline(int page) async {
    final response = await _client.get('${Endpoints.timeline}?page=$page');
    final posts = json.decode(response.body);
    return Page.fromJson(posts, (js) => Post.fromJson(js));
  }
}
