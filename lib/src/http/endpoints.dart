const _BASE_URL = 'https://instalikeback.herokuapp.com';

typedef ParseJson<T> = T Function(Map<String, dynamic> js);

class Endpoints {
  // Users endpoints
  static const USERS = '$_BASE_URL/users';

  static const usersMe = '$USERS/me';

  static String userPicture(int userId) => '$USERS/$userId/picture';

  // Posts endpoints
  static const posts = '$_BASE_URL/posts';

  static const timeline = '$posts/timeline';

  static const postsSaved = '$posts/saved';

  static String postImages(int post, int index) => '$posts/$post/images/$index';
}
