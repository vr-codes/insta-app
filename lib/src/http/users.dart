import 'dart:convert';

import 'package:http/http.dart';
import 'package:instaapp/src/http/endpoints.dart';
import 'package:instaapp/src/model/user.dart';

class Users {

  Client _client = Client();

  loadMe() async {
    final response = await _client.get(Endpoints.usersMe);
    final me = json.decode(response.body);
    return User.fromJson(me);
  }

}


